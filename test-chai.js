const chai = require('chai');
const expect = chai.expect;
const math = require('./math');

describe('Test chai', () => {
    it('should compare by expect', () => {
        expect(1).to.equal(1);
    });
    it('should compare another things by expect', () => {
        expect(5>8).to.be.false;
        expect({name: 'peck'}).to.deep.equal({name: 'peck'});
        expect({}).to.be.a('object');
        expect(1).to.be.a('number');
        expect('peck').to.be.a('string');
        expect('peck'.length).to.equal(4);
        expect('peck').to.lengthOf(4);
        expect([1,2,3]).to.lengthOf(3);
        expect(null).to.be.null;
        expect(undefined).to.not.exist;
        expect(1).to.exist;
    });
});

describe('math module', () => {
    context('function add1', () => {
        it('ควรส่งค่ากลับเป็นเลข', () =>{
            expect(math.add1(0,0)).to.be.a('number');
        } );
        it('add(1,1)ควรส่งค่ากลับเป็น 2', () =>{
            expect(math.add1(1,1)).to.equal(2);
        } );   
    });
});